package com.greatLearning.assignment;

public class AdminDepartment extends SuperDepartment {

	// Declare method departmentName of return type string
	public String deparmentName() {
		return "Admin Department";
	}
	// Declare method getTodaysWork of return type string
	public String getTodaysWork() {
		return "Complete your documents Submission";
	}
	// Declare method getWorkDeadline of return type string
	public String getWorkDeadline() {
		return "Complete by EOD";
	}

}