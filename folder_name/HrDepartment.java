package com.greatLearning.assignment;

public class HrDepartment extends SuperDepartment {
	
	// Declare method departmentName of return type string
	public String deparmentName() {
		return "HR Department";
	}
	// Declare method getTodaysWork of return type string
	public String getTodaysWork() {
		return "Fill todays worksheet and mark your attendance";
	}
	// Declare method doActivity of return type string
	public String doActivity() {
		return "Team Lunch";
	}
	// Declare method getWorkDeadline of return type string
	public String getWorkDeadline() {
		return "Complete by EOD";
	}
	
}