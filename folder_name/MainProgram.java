package com.greatLearning.assignment;

public class MainProgram {

	public static void main(String[] args) {

		// Create the object of AdminDepartment and use all the methods
		AdminDepartment ad = new AdminDepartment();
		System.out.println(ad.deparmentName());
		System.out.println("Today's work : " + ad.getTodaysWork());
		System.out.println("Work Deadline : " + ad.getWorkDeadline());
		System.out.println("About Holiday : " + ad.isTodayAHoliday());
		
		// Create the object of HrDepartment and use all the methods
		HrDepartment hd = new HrDepartment();
		System.out.println(hd.deparmentName());
		System.out.println("Today's work : " + hd.getTodaysWork());
		System.out.println("Activity : " + hd.doActivity());
		System.out.println("Work Deadline : " + hd.getWorkDeadline());
		System.out.println("About Holiday : " + hd.isTodayAHoliday());
		
		// Create the object of TechDepartment and use all the methods
		TechDepartment td = new TechDepartment();
		System.out.println(td.deparmentName());
		System.out.println("Today's work : " + td.getTodaysWork());
		System.out.println("Tech Stack Information : " + td.getTechStackInformation());
		System.out.println("Work Deadline : " + td.getWorkDeadline());
		System.out.println("About Holiday : " + td.isTodayAHoliday());
		
	}

}