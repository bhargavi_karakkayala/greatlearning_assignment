package com.greatLearning.assignment;

public class SuperDepartment {

	// Declare method departmentName of return type string
	public String deparmentName() {
		return "Super Department";
	}
	// Declare method getTodaysWork of return type string
	public String getTodaysWork() {
		return "No work as of now";
	}
	// Declare method getWorkDeadline of return type string
	public String getWorkDeadline() {
		return "Nil";
	}
	// Declare method isTodayAHoliday of type string
	public String isTodayAHoliday() {
		return "Today is not a holiday";
	}
}
