package com.greatLearning.assignment;

public class TechDepartment extends SuperDepartment {
	
	// Declare method departmentName of return type string
	public String deparmentName() {
		return "Tech Department";
	}
	// Declare method getTodaysWork of return type string
	public String getTodaysWork() {
		return "Complete coding of module 1";
	}
	// Declare method getTechStackInformation of return type string
	public String getTechStackInformation() {
		return "Complete by EOD";
	}
	// Declare method getWorkDeadline of return type string
	public String getWorkDeadline() {
		return "core Java";
	}
	
}